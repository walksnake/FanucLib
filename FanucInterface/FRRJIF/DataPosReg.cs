using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataPosReg
    {
        internal DataPosReg()
        {
        }

        public FRIF_DATA_TYPE DataType { get; }

        public int Group { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool GetValue(int Index, ref Array Xyzwpr, ref Array Config, ref Array Joint, ref short UF, ref short UT, ref short ValidC, ref short ValidJ)
        {
            return true;
        }

        public bool SetValueJoint(int Index, ref Array Joint, short UF, short UT)
        {
            return true;
        }

        public bool SetValueXyzwpr(int Index, ref Array Xyzwpr, ref Array Config, short UF, short UT)
        {
            return true;
        }

        public int StartIndex { get; }

        public int EndIndex { get; }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }

        public bool GetValueXyzwpr(int Index, ref float X, ref float Y, ref float Z, ref float W, ref float P, ref float R, ref float E1, ref float E2, ref float E3, ref short C1, ref short C2, ref short C3, ref short C4, ref short C5, ref short C6, ref short C7, ref short UF, ref short UT, ref short ValidC)
        {
            return true;
        }

        public bool GetValueJoint(int Index, ref float J1, ref float J2, ref float J3, ref float J4, ref float J5, ref float J6, ref float J7, ref float J8, ref float J9, ref short UT, ref short ValidJ)
        {
            return true;
        }

        public bool SetValueJoint2(int Index, float J1, float J2, float J3, float J4, float J5, float J6, float J7, float J8, float J9, short UF, short UT)
        {
            return true;
        }

        public bool SetValueXyzwpr2(int Index, float X, float Y, float Z, float W, float P, float R, float E1, float E2, float E3, short C1, short C2, short C3, short C4, short C5, short C6, short C7, short UF, short UT)
        {
            return true;
        }
    }
}