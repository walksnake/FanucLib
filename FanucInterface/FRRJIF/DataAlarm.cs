using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataAlarm
    {
        internal DataAlarm()
        { }

        public FRIF_DATA_TYPE DataType { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool GetValue(int Count, ref short AlarmID, ref short AlarmNumber, ref short CauseAlarmID, ref short CauseAlarmNumber, ref short Severity, ref short Year, ref short Month, ref short Day, ref short Hour, ref short Minute, ref short Second, ref string AlarmMessage, ref string CauseAlarmMessage, ref string SeverityMessage)
        {
            return true;
        }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }
    }
}