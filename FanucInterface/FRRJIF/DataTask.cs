using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataTask
    {
        internal DataTask()
        {
        }

        public FRIF_DATA_TYPE DataType { get; }

        public int Index { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool GetValue(ref string ProgName, ref short LineNumber, ref short State, ref string ParentProgName)
        {
            return true;
        }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }
    }
}