using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataString
    {
        internal DataString()
        {
        }

        public FRIF_DATA_TYPE DataType { get; }

        public int StartIndex { get; }

        public int EndIndex { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool GetValue(int Index, ref string Value)
        {
            return true;
        }

        public bool SetValue(int Index, string Value)
        {
            return true;
        }

        public bool Update()
        {
            return true;
        }

        public void Reset()
        {
        }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }
    }
}